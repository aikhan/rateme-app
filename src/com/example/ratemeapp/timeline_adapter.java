package com.example.ratemeapp;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class timeline_adapter extends BaseAdapter {
               
                private static ArrayList<timeline_detail> itemDetailsrrayList;
               
                LayoutInflater layoutInflator;
                String[] countryName;
                int[] countryFlag;
                Context context;
               
public timeline_adapter(ArrayList<timeline_detail> result , Context c) {
                                // TODO Auto-generated constructor stub
                                itemDetailsrrayList = result;
                                context = c;
                }

                public int getCount() {
                                // TODO Auto-generated method stub
                                return itemDetailsrrayList .size();
                }

                public Object getItem(int arg0) {
                                // TODO Auto-generated method stub
                                return itemDetailsrrayList .get(arg0);
                }

                public long getItemId(int position) {
                                // TODO Auto-generated method stub
                                return position;
                }

                public View getView(int position, View convertView, ViewGroup parent) {
                                // TODO Auto-generated method stub
                               
        layoutInflator  =    
        (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);                               
        View row = layoutInflator.inflate(R.layout.timeline_list, parent,false);
                               
        TextView textview = (TextView) row.findViewById(R.id.textView1);
        ImageView imageview = (ImageView) row.findViewById(R.id.imageView1);

        textview.setText(itemDetailsrrayList .get(position).getName());
        imageview.setImageResource(itemDetailsrrayList .get(position).getImage());

        return (row);
                }
 }

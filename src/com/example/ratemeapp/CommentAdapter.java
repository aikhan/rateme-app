package com.example.ratemeapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
  static int getc = 0;
    private String mediaid;
   //static String comment;
   private ArrayList<String> comment;
 
    public CommentAdapter(Context context,int c,ArrayList<String> com) {
        mInflater = LayoutInflater.from(context);
        //this.mediaid = media;
        this.getc =  c;
        this.comment = com;
        //this.comment = com;
    }
 
    public int getCount() {
        return getc;
    }
 
    public String getItem(int position) {
        return  comment.get(position).toString(); //(person) items.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        //person s = (person) items.get(position);
        if (convertView == null) {
        
        //	jna.res.setText("CONVERT VIEW");
            convertView = mInflater.inflate(R.layout.row, null);
            holder = new ViewHolder();
           
            holder.txtmedia = (TextView) convertView.findViewById(R.id.tvmedia);
            holder.txtcomment = (TextView) convertView.findViewById(R.id.tvcomment);
          //  holder.txtimgt = (TextView) convertView.findViewById(R.id.tvimgt);
           
            holder.image = (ImageView) convertView.findViewById(R.id.img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
          
        }
        
        holder.txtmedia.setText(person.Instance.getmedia());
        holder.txtcomment.setText(comment.get(position));
        
        if (person.Instance.getImage() != null) {
            holder.image.setImageBitmap(person.Instance.getImage());
        } else {
                // MY DEFAULT IMAGE
        	//jna.res.setText("getImage ELSE");
            holder.image.setImageResource(R.drawable.ic_launcher);
        }
        return convertView;
    }
 
    static class ViewHolder {
        TextView txtmedia;
        TextView txtcomment;
        TextView txtuid;
        TextView txtrate;
        TextView txttitle;
        //TextView txtimgt;
        
        ImageView image;
    } 

}

package com.example.ratemeapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class register extends Activity {
	TextView check;
	EditText username, pass, reppass, dob, email, loc, gender;
	private TextView tvDisplayDate;
	private DatePicker dpResult;
	private Button register, button;
	register r;
	private AlertDialog myDialog;
	Context context = this;
	private int year;
	private int month;
	private int day;
	static String getusername, getpass, getreppass, getdob, getgender,
			getemail, getloc, usernameurl = "", status = "", x = "";

	static final int DATE_DIALOG_ID = 999;
	StringBuilder builder = new StringBuilder();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rmregister);
		button = (Button) findViewById(R.id.alertbtn);
		register = (Button) findViewById(R.id.register);
		username = (EditText) findViewById(R.id.tfdisplayname);
		pass = (EditText) findViewById(R.id.tfpass);
		reppass = (EditText) findViewById(R.id.tfreppass);
		dob = (EditText) findViewById(R.id.tfdob);
		gender = (EditText) findViewById(R.id.tfgender);
		email = (EditText) findViewById(R.id.tfemail);
		loc = (EditText) findViewById(R.id.tfloc);
		check = (TextView) findViewById(R.id.check);
		dpResult = (DatePicker) findViewById(R.id.dpResult);
		tvDisplayDate = (TextView) findViewById(R.id.hide);
		setCurrentDateOnView();

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getusername = username.getText().toString();
				getpass = pass.getText().toString();
				getreppass = reppass.getText().toString();
				getdob = dob.getText().toString();
				getgender = gender.getText().toString();
				getemail = email.getText().toString();
				getloc = loc.getText().toString();
				Log.i("PASSWORD", "" + getpass);
				Log.i("REPEATPASSWORD", "" + getreppass);
				if (null != getusername && getusername.length() > 0) {
					//usernameurl = function
						//	.getJsonFormUrl("http://semanticnotion.com/rateme/api/user?$filter=username%20eq%20'"
							//		+ getusername + "'");
					//Log.i("URL is", "" + usernameurl);
					if (null != getpass && getpass.length() > 0) {
						if (null != getreppass && getreppass.length() > 0) {
							if (getpass.equals(getreppass)) {
								Log.i("STATUS", "ENTERED IN COMPARISON");

								if (null != getdob && getdob.length() > 0) {
									if (null != getgender
											&& getgender.length() > 0) {
										if (null != getemail
												&& getemail.length() > 0) {
											if (null != getloc
													&& getloc.length() > 0) {
												if (status.equals("yes")) {
													sendPostRequest(
															getusername,
															getpass, getdob,
															getgender,
															getemail, getloc);
													finish();
												} else {
													Toast.makeText(
															context,
															"Please Enter Valid Username!",
															Toast.LENGTH_LONG)
															.show();
												}
											} else {
												Toast.makeText(
														context,
														"Please Enter Your Location",
														Toast.LENGTH_LONG)
														.show();
											}
										} else {
											Toast.makeText(context,
													"Please Enter Your Email",
													Toast.LENGTH_LONG).show();
										}
									} else {
										Toast.makeText(context,
												"Please Enter your Gender",
												Toast.LENGTH_LONG).show();
									}
								} else {
									Toast.makeText(context,
											"Please Enter your Date Of Birth",
											Toast.LENGTH_LONG).show();
								}
							} else {
								Log.i("IN IF PASSWORD", "" + getpass);
								Log.i("IN IF REPEATPASSWORD", "" + getreppass);
								Toast.makeText(context,
										"Please Enter same pass",
										Toast.LENGTH_LONG).show();
								pass.setText(null);
								reppass.setText(null);
							}
						} else {
							Toast.makeText(context, "Please ReEnter Password",
									Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(context, "Please Enter Password",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(context, "Please Enter UserName",
							Toast.LENGTH_LONG).show();
				}
			}
		});
		username.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if (hasFocus || !hasFocus) {
					if (null != username.getText().toString()
							&& username.getText().toString().length() > 0) {
						x = "http://semanticnotion.com/rateme/api/user?$filter=username%20eq%20'"
								+ username.getText().toString() + "'";
						Log.i("URL is", "" + x);
						Thread t = new Thread(new Runnable() {
							public void run() {
								usernameurl = function.getJsonFormUrl(x);

							}
						});
						t.start();
						//t.destroy();
						
						Log.i("JSON is", "" + usernameurl);

						if (usernameurl.equals("[]")) {
							Log.i("URL is", "EMPTY");
							status = "yes";
							check.setBackgroundResource(R.drawable.yes);
							Toast.makeText(context, "UserName is Valid",
									Toast.LENGTH_LONG).show();

						} else {
							Log.i("URL has", "DATA");
							status = "no";
							check.setBackgroundResource(R.drawable.no);
							UsernameAlert();
							Toast.makeText(context, "UserName Already Exist",
									Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(context, "UserName not entered",
								Toast.LENGTH_LONG).show();
						check.setBackgroundResource(0);
					}
				}
			}
		});
		dob.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);
			}
		});
		dob.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					showDialog(DATE_DIALOG_ID);
				}
			}
		});
		gender.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					showDialogButtonClick();
				}
			}
		});
		gender.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialogButtonClick();
			}
		});
	}

	public void showDialogButtonClick() {
		Log.i("ALERT", "show Dialog ButtonClick");
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Select Your Gender");

		final CharSequence[] choiceList = { "Male", "Female" };

		int selected = -1; // does not select anything

		builder.setSingleChoiceItems(choiceList, selected,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						gender.setText("");
						gender.setText(choiceList[which]);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void sendPostRequest(String givenUsername, String givenPass,
			String givenDob, String givenGender, String givenEmail,
			String givenLoc) {

		class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

			@Override
			protected String doInBackground(String... params) {

				String paramUsername = params[0];
				String paramPass = params[1];
				String paramDob = params[2];
				String paramGender = params[3];
				String paramEmail = params[4];
				String paramLoc = params[5];

				Log.i("doInBackground", "paramUsername" + paramUsername);

				HttpClient httpClient = new DefaultHttpClient();

				// In a POST request, we don't pass the values in the URL.
				// Therefore we use only the web page URL as the parameter of
				// the HttpPost argument
				HttpPost httpPost = new HttpPost(
						"http://semanticnotion.com/rateme/api/user");
				Log.i("SENDING POST", "CALLED URL");
				// Because we are not passing values over the URL, we should
				// have a mechanism to pass the values that can be
				// uniquely separate by the other end.
				// To achieve that we use BasicNameValuePair
				// Things we need to pass with the POST request
				BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair(
						"username", paramUsername);
				BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair(
						"password", paramPass);
				BasicNameValuePair DobBasicNameValuePAir = new BasicNameValuePair(
						"dob", paramDob);
				BasicNameValuePair GenderBasicNameValuePAir = new BasicNameValuePair(
						"sex", paramGender);
				BasicNameValuePair EmailBasicNameValuePAir = new BasicNameValuePair(
						"email", paramEmail);
				BasicNameValuePair LocBasicNameValuePAir = new BasicNameValuePair(
						"location", paramLoc);

				// We add the content that we want to pass with the POST request
				// to as name-value pairs
				// Now we put those sending details to an ArrayList with type
				// safe of NameValuePair
				List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
				nameValuePairList.add(usernameBasicNameValuePair);
				nameValuePairList.add(passwordBasicNameValuePAir);
				nameValuePairList.add(DobBasicNameValuePAir);
				nameValuePairList.add(GenderBasicNameValuePAir);
				nameValuePairList.add(EmailBasicNameValuePAir);
				nameValuePairList.add(LocBasicNameValuePAir);

				Log.i("LIST", "NAME ADDED TO LIST");
				try {
					// UrlEncodedFormEntity is an entity composed of a list of
					// url-encoded pairs.
					// This is typically useful while sending an HTTP POST
					// request.
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
							nameValuePairList);

					// setEntity() hands the entity (here it is
					// urlEncodedFormEntity) to the request.
					httpPost.setEntity(urlEncodedFormEntity);

					try {
						// HttpResponse is an interface just like HttpPost.
						// Therefore we can't initialize them
						HttpResponse httpResponse = httpClient
								.execute(httpPost);
						Log.i("HTTP RESPONSE", "CALLED FOR RESPONSE");
						// According to the JAVA API, InputStream constructor do
						// nothing.
						// So we can't initialize InputStream although it is not
						// an interface
						InputStream inputStream = httpResponse.getEntity()
								.getContent();

						InputStreamReader inputStreamReader = new InputStreamReader(
								inputStream);

						BufferedReader bufferedReader = new BufferedReader(
								inputStreamReader);

						StringBuilder stringBuilder = new StringBuilder();

						String bufferedStrChunk = null;

						while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
							stringBuilder.append(bufferedStrChunk);
							Log.i("BUFFERED CHUNK", "ENTERED IN WHILE");
						}

						return stringBuilder.toString();

					} catch (ClientProtocolException cpe) {
						System.out
								.println("First Exception caz of HttpResponese :"
										+ cpe);
						cpe.printStackTrace();
					} catch (IOException ioe) {
						System.out
								.println("Second Exception caz of HttpResponse :"
										+ ioe);
						ioe.printStackTrace();
					}

				} catch (UnsupportedEncodingException uee) {
					System.out
							.println("An Exception given because of UrlEncodedFormEntity argument :"
									+ uee);
					uee.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);

				if (result.equals("working")) {
					Toast.makeText(getApplicationContext(),
							"HTTP POST is working...", Toast.LENGTH_LONG)
							.show();
				} else {
					Toast.makeText(getApplicationContext(), "DataSent",
							Toast.LENGTH_LONG).show();
				}
			}
		}

		SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
		sendPostReqAsyncTask.execute(givenUsername, givenPass, givenDob,
				givenGender, givenEmail, givenLoc);
	}

	public void UsernameAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(register.this);
		builder.setTitle("USERNAME Error");
		builder.setMessage("Username alredy exists, please choose another one!");
		builder.setIcon(R.drawable.no);

		builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				username.setText("");
			}
		});

		builder.setCancelable(false);
		myDialog = builder.create();
		myDialog.show();

	}

	public void setCurrentDateOnView() {

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// set date picker as current date
			return new DatePickerDialog(this, datePickerListener, year, month,
					day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// set selected date into tfdob
			dob.setText(builder.delete(0, 10));
			dob.setText(builder.append(month + 1).append("-").append(day)
					.append("-").append(year).append(" ").toString());

			// set selected date into datepicker also
			dpResult.init(year, month, day, null);

		}
	};
}

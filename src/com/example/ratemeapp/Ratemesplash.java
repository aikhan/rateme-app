package com.example.ratemeapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class Ratemesplash extends Activity {
	static int c = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		setContentView(R.layout.ratemesplash);
		
		if (c == 0) {
			c = c + 1;
			final Context context = this;
			Thread timer = new Thread() {
				public void run() {
					try {
						sleep(2000);
					} catch (Exception e) {
						e.getStackTrace();
					} finally {
						Intent open = new Intent(context, Ratemeactivity.class);
						startActivity(open);

					}
				}
			};
			timer.start();

		}else{
			System.exit(0);
		}
	}

}

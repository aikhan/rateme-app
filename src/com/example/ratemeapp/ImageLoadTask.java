package com.example.ratemeapp;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class ImageLoadTask extends AsyncTask<String, String, Bitmap> {
	imagepage ip;
	profile prof;
	Ratemeactivity rma;
	String s, finalurl,ch;
	comment com;
	int count = 0,c = 0;
	public Bitmap image;
	@Override
	protected void onPreExecute() {
		Log.i("ImageLoadTask", "Loading image...");
	}

	// PARAM[0] IS IMG URL
	protected Bitmap doInBackground(String... param) {

		try {
			//ch = getmedia();
		//	s = param[0].toString();
		//	finalurl = s.substring(6);

			Log.i("ImageLoadTask", "Attempting to load image URL: "
					+param[0]);
			count = count + 1;
			URL url = new URL(param[0]);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	protected void onProgressUpdate(String... progress) {
		// NO OP
	}

	protected void onPostExecute(Bitmap ret) {
	
		if (ret != null) {
		//	int t = (int)Thread.currentThread().getId();
			Log.i("ImageLoadTask",
					"Successfully loaded " + person.Instance.getmedia());
			image = ret;
			
			Bitmap bitbit = Bitmap.createBitmap(ret);
			person.Instance.setImage(bitbit);
			ip = new imagepage();
			ip.imagepagebit.add(bitbit);
			prof.profilebit.add(bitbit);
			rma.ratemebit.add(bitbit);
			Log.i("IMAGEPAGEBIT>SIZE()", "is" + ip.imagepagebit.size());
			Log.i("RATEMEACTIVUTYBIT>SIZE()", "is" + rma.ratemebit.size());
			Log.i("PROFILEBIT>SIZE()", "is" + prof.profilebit.size());
			c = c+1;
			Log.i("ImageLoadTask",
					"Value of Media is "+ ch );
			if(c ==5){
				Log.i("ImageLoadTask",
						"ENTERED IN C == 5" );
				
			}
		//	Log.i("THREAD INFO", "THREAD ID is " + t);
			

		} else {
			Log.e("ImageLoadTask", "Failed to load " +person.Instance.getmedia() + " image");

		}
	}
}
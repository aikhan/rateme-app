package com.example.ratemeapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class topratedboys extends Activity {

	ArrayAdapter<String> adapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.topratedboys);
		ListView list = (ListView) findViewById(R.id.list1);
		String[] days = { "Top Of the Day", "Top Of the Week",
				"Top Of the Month", "Top Of the Year", "Top of all Time",
				"NearBy", "Most Followed", "Recent", "Most Commented" };
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, days);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Log.i("ITEM SELECTED",
						"Selected Item is "
								+ adapter.getPosition("Top Of the Day"));
				if (position == 0) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 1) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 2) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 3) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 4) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 5) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 6) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 7) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				} else if (position == 8) {
					Intent open = new Intent(topratedboys.this, imagepage.class);
					startActivity(open);
				}

			}
		});
	}
}

package com.example.ratemeapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends Activity {
	Button signup, login;
	private EditText usernameEditText;
	private EditText passwordEditText;
	protected Ratemeactivity  rma;
	String givenUsername,givenPassword,logincheck;
	 // Email, password edittext
    EditText txtUsername, txtPassword;
    // Ratemeactivity rma;
    // login button
    Button btnLogin;
     
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
     
    // Session Manager Class
    SessionManagment session;
	// private Button sendPostReqButton;
	// private Button clearButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		signup = (Button) findViewById(R.id.Signupbtn);
		login = (Button) findViewById(R.id.loginbtn);
		usernameEditText = (EditText) findViewById(R.id.loginusername);
		passwordEditText = (EditText) findViewById(R.id.loginuserpass);

		signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myintent = new Intent(login.this, register.class);
				startActivity(myintent);
			}
		});
		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 givenUsername = usernameEditText.getEditableText().toString();
				 givenPassword = passwordEditText.getEditableText().toString();
				 Log.i("INPUT", "Given username :" + givenUsername
						+ " Given password :"+givenPassword);
				// logincheck = function
					//		.getJsonFormUrl("http://192.168.1.10:1360/test/api/user?un="
						//			+ givenUsername);
				 if(givenUsername.trim().length() > 0 && givenPassword.trim().length() > 0){
	                    
	                        new SessionManagment(login.this).createLoginSession(givenUsername, givenPassword);
	                         Log.i("LOGIN INFO",new SessionManagment(login.this).getUserDetails().toString());
	                         person.Instance.setUsername(givenUsername);
	                      Ratemeactivity r = new Ratemeactivity();
	                      r.getuservalue = givenUsername;
	                      r.btnnamechange = "Logout";
	                      Log.i("LOGIN FILE" ," VALUE OF USER IS " +r.getuservalue);
	                  //    r.setuser();
	                     // r.finish();
	                   
	                       Intent i = new Intent(getApplicationContext(), Ratemeactivity.class);
	                       startActivity(i);
	                        finish();
	                         
	                                   
	                }else{
	                    // user didn't entered username or password
	                    // Show alert asking him to enter the details
	                    alert.showAlertDialog(login.this, "Login failed..", "Please enter username and password", false);
	                }
				}

		});
	}

	
}

package com.example.ratemeapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class imagepage extends Activity {
	Button info, comment;
	RatingBar ratingbar;
	TextView display;
	RelativeLayout lay;
	Context context = this;
	int width, height;
	int c = 0;
	static ArrayList<Bitmap> imagepagebit = new ArrayList<Bitmap>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imagepage);
		final Context context = this;
		ratingbar = (RatingBar) findViewById(R.id.ratingBar1);
		info = (Button) findViewById(R.id.info);
		lay = (RelativeLayout) findViewById(R.id.layout);
		comment = (Button) findViewById(R.id.comments);
		display = (TextView) findViewById(R.id.textView1);

		ratingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {

				display.setText(String.valueOf(rating * 2));

			}
		});
		info.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent open = new Intent(context, profile.class);
				startActivity(open);
			}
		});
		comment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent open = new Intent(context, comment.class);
				startActivity(open);
			}
		});

		ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
		ImagePagerAdapter adapter = new ImagePagerAdapter();
		viewPager.setAdapter(adapter);
	}

	public class ImagePagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imagepagebit.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			final Context context = imagepage.this;

			ImageView imageView = new ImageView(context);
			int padding = context.getResources().getDimensionPixelSize(
					R.dimen.activity_vertical_margin);
			imageView.setPadding(padding, padding, padding, padding);
			imageView.setImageBitmap(imagepagebit.get(position));
			((ViewPager) container).addView(imageView, 0);
			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					myView();
				}
			});

			c = position;
			if (position == c || position != c) {

				ratingbar.setRating((float) 0.0);

			}

			return imageView;

		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {

			((ViewPager) container).removeView((ImageView) object);
			ratingbar.setRating((float) 0.0);

		}

	}

	public View myView() {
		View v = new View(context); // Creating an instance for View Object
		LayoutInflater inflater = (LayoutInflater) getBaseContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = inflater.inflate(R.layout.dialog_iv, null);
		ImageView bigview = (ImageView) v.findViewById(R.id.dialog_iv);
		lay.addView(v);
		return v;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ratemeactivity, menu);
		return true;
	}
}

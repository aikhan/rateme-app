package com.example.ratemeapp;

import java.util.ArrayList;
import android.app.Activity;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class timeline_main extends Activity {
               
                String[] text = { "Afghanistan", "Algeria", "Australia", "Bermuda", "Bhutan", "Canada", "China","India" };

                int[] image = { R.drawable.image1, R.drawable.image2, R.drawable.image3,
            R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7,
            R.drawable.image8 };

                timeline_detail item_details;
                /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline_main);
       
        ArrayList<timeline_detail> result = GetSearchResults();
        ListView lv = (ListView)findViewById(R.id.listView1);
        lv.setAdapter(new timeline_adapter(result,getApplicationContext()));
       
    }
                private ArrayList<timeline_detail> GetSearchResults() {
                                // TODO Auto-generated method stub
                                ArrayList<timeline_detail> results = new ArrayList<timeline_detail>();
                                 
                                for(int i=0;i<text.length;i++)
                                {
                                                item_details= new timeline_detail();
                                                item_details.setName(text[i]);
                                                item_details.setImage(image[i]);
                                                results.add(item_details);
                                }
                               
                                return results;
                }
}


package com.example.ratemeapp;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.DrawableContainer;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Ratemeactivity extends Activity {
	
	Button topratedgirls, topratedboys, random, timeline, register,login,logout;
	HorizontalScrollView myScroller, myScroller2;
	Resources res = null;
	static int c = 0;
	ArrayAdapter list;
	static String ret = null, check = null;
	static String getuservalue="Username",btnnamechange="Login/Signup";
	ProgressDialog mProgressDialog;
	TextView tv,username,pass;
	ImageView iv2, img, imageView;
	LinearLayout lay1, lay2, ll1;
	Context context = this;
	int value = 0, fixvalue = 0, value2 = 0, fixvalue2 = 0;
	static ArrayList<person> results = new ArrayList<person>();
	static ArrayList<Bitmap> ratemebit = new ArrayList<Bitmap>();
	public static int count = 0;
	
	//SessionManagment sm = new SessionManagment(context);
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ratemeactivity);
		final SessionManagment sm = new SessionManagment(Ratemeactivity.this);
		//new DownloadJSON().execute();
		//new picJsonLoad().execute();
		tv = (TextView) findViewById(R.id.textView1);
		username = (TextView) findViewById(R.id.displayusername);
		if(sm.getUserDetails().equals("{email=null,name=null}")){
		username.setText(getuservalue);
		Log.i("CHECK", "You need to b logged in");
		
		Log.i("CHECK", ""+ sm.getUserDetails());
		}else{
			Log.i("CHECK", "Tou were Logged In"+sm.getUserDetails());	
			
		}
		//pass = (TextView) findViewById(R.id.displaypass);
		lay1 = (LinearLayout) findViewById(R.id.images);
		topratedgirls = (Button) findViewById(R.id.trg);
		topratedboys = (Button) findViewById(R.id.trb);
		random = (Button) findViewById(R.id.rand);
		login = (Button)findViewById(R.id.login);
		logout = (Button)findViewById(R.id.logout);
		random.setText("Click");
		timeline = (Button) findViewById(R.id.tl);
		register = (Button) findViewById(R.id.reg);
		register.setText(btnnamechange);
		myScroller = new HorizontalScrollView(this);
		myScroller2 = new HorizontalScrollView(this);
		res = this.getResources();
		lay2 = (LinearLayout) findViewById(R.id.images2);
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				sm.checkLogin();
			}
		});
		logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sm.logoutUser();
			}
		});
		random.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) { // TODO Auto-generated method stub
			displayimg();
			displaybottomimg();
			}
		});

		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(btnnamechange.equals("Logout")){
					sm.logoutUser();
					Log.i("LOGOUT STATUS","You are LoggedOut Succesfully");
					btnnamechange = "Login/Signup";
					register.setText("Login/Signup");
					finish();
				}else{
					sm.checkLogin();
				 }

			}
		});
		topratedgirls.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myintent = new Intent(context, topratedgirls.class);
				startActivity(myintent);

			}
		});
		topratedboys.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myintent = new Intent(context, topratedboys.class);
				startActivity(myintent);
			}
		});

		timeline.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent myintent = new Intent(context, timeline_main.class);
				startActivity(myintent);
			}
		});
	}
	
	public void startdownload() {
		try { 
			JSONArray ja = new JSONArray(ret); 
			for (int i = 0; i < ja.length(); i++) {
			//	person p = new person();
				
				JSONObject jo = (JSONObject) ja.get(i);
				String m = null, uid = null, t, imgt, imgl, r = null;				
				m = jo.getString("fname");//media--->firstname
				uid = jo.getString("iduser"); 
				t = jo.getString("sex"); //title--->sex
				//imgt = jo.getString("thumb");
				//imgl = jo.getString("large");
				r = jo.getString("followers"); //rating
				
				person.Instance.setmedia("Name:	" + m);
				person.Instance.settitle("Gender:	" + t);
				person.Instance.setuserid("User_ID:	" + uid);
				person.Instance.setrating("Followers:	" + r);
				//person.Instance.setimgthumb("Thumb:	" + imgt);
				//person.Instance.setimglarge("large:	" + imgl);
				results.add(person.Instance);
				Log.i("RESULTS ARRAY LIST", "SIZE IS" + results.size());
				Log.i("DATABASE DATA", person.Instance.getuserid());
				Log.i("DATABASE DATA", person.Instance.getmedia());
				Log.i("DATABASE DATA", person.Instance.gettitle());
				Log.i("DATABASE DATA", person.Instance.getrating());
				
				Log.i("RESULTS ARRAY LIST", "SIZE IS" + results.size());
			//	person.Instance.loadImage();
				
			}

	//		for (person s : results) { // START LOADING IMAGES FOR EACH media
									
		//		s.loadImage(); // }
			
		//	}

			// tv.setText(sta.toString());
		} catch (JSONException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}
	public void displayimg() {
		if (ratemebit.size() == 0) {
			Log.i("OOOPS", "BIT.SIZE() is 0");
			
			
		} else {
			Log.i("INDISPLAYIMG BIT>SIZE()", "is" + ratemebit.size());
			for (int i = 0; i < ratemebit.size(); i++) {
				
				ImageView iv = new ImageView(this);
				value = value + 37;
				iv.setAdjustViewBounds(true);
				iv.setImageBitmap(ratemebit.get(i)); // myScroller.addView(iv);
				lay1.addView(iv);
				
			}
			scrollWhenAvailable(1);
		}
	}

	public void displaybottomimg() {
		if (ratemebit.size() == 0) {
			Log.i("OOOPS", "BIT.SIZE() is 0");
		} else {
			Log.i("INDISPLAYIMG BIT>SIZE()", "is" + ratemebit.size());
			for (int i = 1; i < ratemebit.size(); i++) {
				
				ImageView iv2 = new ImageView(this);
			//	value = value + 35;
				iv2.setAdjustViewBounds(true);
				iv2.setImageBitmap(ratemebit.get(i)); // myScroller.addView(iv);
				lay2.addView(iv2);
			}
			scrollWhenAvailabledown(1);
		}
	}
	public void setuser(){
		
	
	}	
	
	public void scrollWhenAvailable(final int x) {
		if (x == myScroller.getScrollX())
			return;

		lay1.scrollBy(x, 0);
		lay1.postDelayed(new Runnable() {
			@Override
			public void run() {
				scrollWhenAvailable(x);
				// tv.setText("" + sv.getScrollX());
				if (lay1.getScrollX() == value) {
					lay1.scrollTo(0, 0);
					// scrollWhenAvailable(x);
				}
			}
		}, 100);

	}

	public void scrollWhenAvailabledown(final int x) {
		if (x == myScroller2.getScrollX())
			return;

		lay2.scrollBy(x, 0);
		lay2.postDelayed(new Runnable() {
			@Override
			public void run() {
				scrollWhenAvailabledown(x);
				// tv.setText("" + sv.getScrollX());
				if (lay2.getScrollX() == value) {
					lay2.scrollTo(0, 0);
					// scrollWhenAvailable(x);
				}
			}
		}, 100);
	}

	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(Ratemeactivity.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Saad JSONParsing");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading Data From WebServer...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			
			ret = function
					.getJsonFormUrl("http://semanticnotion.com/rateme/api/user");
			check = ret;

			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			mProgressDialog.dismiss();
			Toast.makeText(Ratemeactivity.this, "PROCESS COMPLETE! ",
					Toast.LENGTH_LONG).show();
			//Log.i("JSON IS",""+ret.toString());
			startdownload();
			
		
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ratemeactivity, menu);
		return true;
	}

}

package com.example.ratemeapp;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public enum person{
Instance,ins2;
	private Bitmap image;
	private String username="";
	private String DOB = "";
	private String media = "";
	private String title = "";
	private String userid = "";
	private String imgthumb = "";
	private String imglarge = "";
	private String rating = "";
	private String comment = "";
	
private  person(){
	
}
	
	public void setmedia(String m) {
		this.media = m;
		

	}

	public String getmedia() {
		
		return media;

	}

	public void setuserid(String uid) {
		this.userid = uid;

	}

	public String getuserid() {
		return userid;

	}

	public void settitle(String t) {
		this.title = t;

	}

	public String gettitle() {
		return title;

	}
	public void setImage(Bitmap b){
		image = b;
	}
	public Bitmap getImage(){
		return image;
		
	}

	public void setimgthumb(String imgt) {
		this.imgthumb = imgt;

	}

	public String getimgthumb() {
		return imgthumb;

	}

	public void setimglarge(String imgl) {
		this.imglarge = imgl;

	}

	public String getimglarge() {
		return imglarge;

	}

	public void setrating(String r) {
		this.rating = r;

	}

	public String getrating() {
		return rating;

	}
	public void setComment(String com){
		this.comment = com;
		
	}
	public String getComment(){
		return comment;
	}
	public void setDOB(String dob){
		this.DOB = dob;
	}
	public String getDOB(){
		return DOB;
	}
	public void setUsername(String un){
		this.username = un;
		
	}
	public String getUsername(){
		return username;
	}
	public void loadImage(String url) {
		// HOLD A REFERENCE TO THE ADAPTER
		
		if (url != null && !url.equals("")) {
			new ImageLoadTask().execute(url);
			
		}
	}

	

}

package com.example.ratemeapp;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class profile extends Activity {

	static ArrayList<Bitmap> profilebit = new ArrayList<Bitmap>();
	LinearLayout lay1;
	HorizontalScrollView sv;
	Button follow;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		lay1 = (LinearLayout) findViewById(R.id.prifleimages);
		follow = (Button)findViewById(R.id.folow);
		sv = (HorizontalScrollView)findViewById(R.id.scrollView2);
		
		follow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (profilebit.size() == 0) {
					Log.i("OOOPS", "profileBIT.SIZE() is 0");
				} else {
					Log.i("IN PROFILE CLASS BIT>SIZE()", "is" + profilebit.size());
					for (int i = 1; i < profilebit.size(); i++) {

						ImageView iv = new ImageView(profile.this);
						// value = value + 35;
						iv.setAdjustViewBounds(true);
						iv.setImageBitmap(profilebit.get(i)); // myScroller.addView(iv);
						lay1.addView(iv);
					}
					scrollWhenAvailabledown2(1);
				}
			}

			public void scrollWhenAvailabledown2(final int x) {
				if (x == sv.getScrollX())
					return;

				lay1.scrollBy(x, 0);
				lay1.postDelayed(new Runnable() {
					@Override
					public void run() {
						scrollWhenAvailabledown2(x);
						// tv.setText("" + sv.getScrollX());
						if (lay1.getScrollX() == 40) {
							lay1.scrollTo(0, 0);
							// scrollWhenAvailable(x);
						}
					}
				}, 100);
			}
			
		});
		
		
	
}}

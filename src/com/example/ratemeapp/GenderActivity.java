package com.example.ratemeapp;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class GenderActivity extends Activity implements OnClickListener {
	private static final String TAG = "DialogDemo";
	private Button showDialogButton;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "Activity start");
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_gender);
		mContext = this; // to use all around this class

		initViewAction();

	}



	private void initViewAction() {
		showDialogButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {

		if (view.equals(showDialogButton)) {
			showDialogButtonClick();
		}
	}

	public void showDialogButtonClick() {
		Log.i(TAG, "show Dialog ButtonClick");
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle("Show dialog");

		final CharSequence[] choiceList = { "Male", "Female"};

		int selected = -1; // does not select anything

		builder.setSingleChoiceItems(choiceList, selected,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(mContext, "Select " + choiceList[which],
								Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	

}
